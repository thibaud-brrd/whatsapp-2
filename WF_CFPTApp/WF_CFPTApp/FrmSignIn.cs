﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_CFPTApp
{
    public partial class FrmSignIn : Form
    {
        // Constantes
        #region Constantes

        #endregion

        // Champs
        #region Champs
        SQLFunction _db;
        string _email;
        string _password;
        string _errorMsg;

        #endregion

        // Porpriétés
        #region Propriétés
        internal SQLFunction Db { get => _db; set => _db = value; }

        public string Email { get => _email; set => _email = value; }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = HashWithSHA256(value);
            }
        }

        public string ErrorMsg { get => _errorMsg; set => _errorMsg = value; }

        #endregion

        // Constructeurs
        #region Constructeurs
        public FrmSignIn()
        {
            Db = new SQLFunction();
            Email = "";
            Password = "";
            ErrorMsg = "";

            InitializeComponent();
        }

        #endregion

        // Méthodes
        #region Méthodes
        private void FrmSignIn_Load(object sender, EventArgs e)
        {
            refreshInputText();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            Email = tbxEmail.Text;
            Password = tbxPassword.Text;
            ValidateEntre();
        }

        public void refreshInputText(){
            tbxEmail.Text    = Email;
            tbxPassword.Text = "";
            lblError.Text    = ErrorMsg;
        }

        public void ValidateEntre()
        {
            if (Db.SelectAllUsers()[1].Contains(Email) && Password == Db.lireUtilisateur(Email).password.ToString())
            {
                lblError.Text = "successful connection";
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                ErrorMsg = "password or username incorect";
                refreshInputText();
            }
        }

        // Encyrpte une chaine de caractères avec sha256
        public static String HashWithSHA256(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        #endregion
    }
}
