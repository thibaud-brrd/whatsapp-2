﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace WF_CFPTApp
{
    class ApiHelper
    {
        public static HttpClient ApiClient { get; set; }

        public static void InitializeClient()
        {
            ApiClient = new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            ApiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Authorization", "Bearer 10100101010");
            ApiClient.BaseAddress = new Uri("https://cfptapp.miyoki.ch/"); // Change here the prefix
        }

        /// <summary>
        /// HTTP Get method to get datas from a WEB API
        /// </summary>
        /// <typeparam name="T"></typeparam> Permits to ask any type to the method
        /// <param name="url"></param> Url of the API
        /// <returns></returns>
        public static async Task<T> GetAsync<T>(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = ApiClient.BaseAddress;
                client.DefaultRequestHeaders.Authorization = ApiClient.DefaultRequestHeaders.Authorization;

                var result = await client.GetAsync(url);

                if (result.IsSuccessStatusCode)
                {
                    T objectReceived = await result.Content.ReadAsAsync<T>();

                    return objectReceived;
                }
                else
                {
                    throw new Exception(result.ReasonPhrase);
                }
            }
        }

        /// <summary>
        /// HTTP Post method to send an object encoded in JSON to a WEB API
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToSerialize"> Object that you want to encode in JSON </param>
        /// <param name="url"></param>
        /// <returns>Nothing</returns>
        public static async Task PostAsync<T>(T objectToSerialize, string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = ApiClient.BaseAddress;
                client.DefaultRequestHeaders.Authorization = ApiClient.DefaultRequestHeaders.Authorization;

                var test = JsonConvert.SerializeObject(objectToSerialize);
                var content = new StringContent(test, Encoding.UTF8, "application/json");

                var result = await client.PostAsync(url, content);
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.ReasonPhrase);
                }
            }
        }

        public static async Task DeleteAsync(string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = ApiClient.BaseAddress;
                client.DefaultRequestHeaders.Authorization = ApiClient.DefaultRequestHeaders.Authorization;

                var result = await client.DeleteAsync(url);
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.ReasonPhrase);
                }
            }
        }

        public static async Task PutAsync<T>(T objectToSerialize, string url)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = ApiClient.BaseAddress;
                client.DefaultRequestHeaders.Authorization = ApiClient.DefaultRequestHeaders.Authorization;

                var test = JsonConvert.SerializeObject(objectToSerialize);
                var content = new StringContent(test, Encoding.UTF8, "application/json");

                var result = await client.PutAsync(url, content);
                string resultContent = await result.Content.ReadAsStringAsync();
                Console.WriteLine(resultContent);

                if (!result.IsSuccessStatusCode)
                {
                    throw new Exception(result.ReasonPhrase);
                }
            }
        }
    }
}