﻿namespace WF_CFPTApp
{
    partial class FrmDiscussion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msDiscussion = new System.Windows.Forms.MenuStrip();
            this.tsmCompte = new System.Windows.Forms.ToolStripMenuItem();
            this.deconnexionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDiscussions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.pibLogo = new System.Windows.Forms.PictureBox();
            this.gbVisuelMessage = new System.Windows.Forms.GroupBox();
            this.flpListMsg = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblcontacts = new System.Windows.Forms.Label();
            this.lsbContacts = new System.Windows.Forms.ListBox();
            this.tbxEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.btnAddFriend = new System.Windows.Forms.Button();
            this.tbxSendMessage = new System.Windows.Forms.TextBox();
            this.btnSendMessage = new System.Windows.Forms.Button();
            this.msDiscussion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).BeginInit();
            this.gbVisuelMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // msDiscussion
            // 
            this.msDiscussion.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCompte,
            this.tsmDiscussions,
            this.tsmHelp});
            this.msDiscussion.Location = new System.Drawing.Point(0, 0);
            this.msDiscussion.Name = "msDiscussion";
            this.msDiscussion.Size = new System.Drawing.Size(1264, 24);
            this.msDiscussion.TabIndex = 0;
            this.msDiscussion.Text = "menuStrip1";
            // 
            // tsmCompte
            // 
            this.tsmCompte.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deconnexionToolStripMenuItem});
            this.tsmCompte.Name = "tsmCompte";
            this.tsmCompte.Size = new System.Drawing.Size(62, 20);
            this.tsmCompte.Text = "Compte";
            // 
            // deconnexionToolStripMenuItem
            // 
            this.deconnexionToolStripMenuItem.Name = "deconnexionToolStripMenuItem";
            this.deconnexionToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
            this.deconnexionToolStripMenuItem.Text = "Deconnexion";
            this.deconnexionToolStripMenuItem.Click += new System.EventHandler(this.deconnexionToolStripMenuItem_Click);
            // 
            // tsmDiscussions
            // 
            this.tsmDiscussions.Name = "tsmDiscussions";
            this.tsmDiscussions.Size = new System.Drawing.Size(80, 20);
            this.tsmDiscussions.Text = "Discussions";
            // 
            // tsmHelp
            // 
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmHelp.Text = "Help";
            // 
            // pibLogo
            // 
            this.pibLogo.Image = global::WF_CFPTApp.Properties.Resources.logo2;
            this.pibLogo.Location = new System.Drawing.Point(12, 27);
            this.pibLogo.Name = "pibLogo";
            this.pibLogo.Size = new System.Drawing.Size(130, 130);
            this.pibLogo.TabIndex = 1;
            this.pibLogo.TabStop = false;
            // 
            // gbVisuelMessage
            // 
            this.gbVisuelMessage.BackColor = System.Drawing.Color.Transparent;
            this.gbVisuelMessage.Controls.Add(this.flpListMsg);
            this.gbVisuelMessage.Location = new System.Drawing.Point(226, 45);
            this.gbVisuelMessage.Name = "gbVisuelMessage";
            this.gbVisuelMessage.Size = new System.Drawing.Size(1026, 534);
            this.gbVisuelMessage.TabIndex = 0;
            this.gbVisuelMessage.TabStop = false;
            this.gbVisuelMessage.Text = "Name Friend";
            // 
            // flpListMsg
            // 
            this.flpListMsg.AutoScroll = true;
            this.flpListMsg.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpListMsg.Location = new System.Drawing.Point(6, 19);
            this.flpListMsg.Name = "flpListMsg";
            this.flpListMsg.Size = new System.Drawing.Size(1014, 499);
            this.flpListMsg.TabIndex = 3;
            this.flpListMsg.WrapContents = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Right;
            this.label2.Location = new System.Drawing.Point(958, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 27);
            this.label2.TabIndex = 1;
            this.label2.Text = "label2";
            // 
            // lblcontacts
            // 
            this.lblcontacts.AutoSize = true;
            this.lblcontacts.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcontacts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblcontacts.Location = new System.Drawing.Point(44, 171);
            this.lblcontacts.Name = "lblcontacts";
            this.lblcontacts.Size = new System.Drawing.Size(144, 37);
            this.lblcontacts.TabIndex = 11;
            this.lblcontacts.Text = "Contacts";
            // 
            // lsbContacts
            // 
            this.lsbContacts.FormattingEnabled = true;
            this.lsbContacts.Location = new System.Drawing.Point(12, 211);
            this.lsbContacts.Name = "lsbContacts";
            this.lsbContacts.Size = new System.Drawing.Size(208, 368);
            this.lsbContacts.TabIndex = 1;
            // 
            // tbxEmail
            // 
            this.tbxEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxEmail.Location = new System.Drawing.Point(19, 630);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(226, 32);
            this.tbxEmail.TabIndex = 2;
            this.tbxEmail.TextChanged += new System.EventHandler(this.tbxEmail_TextChanged);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(12, 590);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(97, 37);
            this.lblEmail.TabIndex = 11;
            this.lblEmail.Text = "Email";
            // 
            // btnAddFriend
            // 
            this.btnAddFriend.BackColor = System.Drawing.Color.Red;
            this.btnAddFriend.Enabled = false;
            this.btnAddFriend.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnAddFriend.ForeColor = System.Drawing.Color.White;
            this.btnAddFriend.Location = new System.Drawing.Point(261, 626);
            this.btnAddFriend.Name = "btnAddFriend";
            this.btnAddFriend.Size = new System.Drawing.Size(165, 40);
            this.btnAddFriend.TabIndex = 3;
            this.btnAddFriend.Text = "Add friend";
            this.btnAddFriend.UseVisualStyleBackColor = false;
            // 
            // tbxSendMessage
            // 
            this.tbxSendMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxSendMessage.Location = new System.Drawing.Point(226, 585);
            this.tbxSendMessage.Name = "tbxSendMessage";
            this.tbxSendMessage.Size = new System.Drawing.Size(1026, 32);
            this.tbxSendMessage.TabIndex = 12;
            // 
            // btnSendMessage
            // 
            this.btnSendMessage.Location = new System.Drawing.Point(1073, 623);
            this.btnSendMessage.Name = "btnSendMessage";
            this.btnSendMessage.Size = new System.Drawing.Size(179, 39);
            this.btnSendMessage.TabIndex = 13;
            this.btnSendMessage.Text = "Send Message";
            this.btnSendMessage.UseVisualStyleBackColor = true;
            this.btnSendMessage.Click += new System.EventHandler(this.btnSendMessage_Click);
            // 
            // FrmDiscussion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnSendMessage);
            this.Controls.Add(this.tbxSendMessage);
            this.Controls.Add(this.btnAddFriend);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.tbxEmail);
            this.Controls.Add(this.lsbContacts);
            this.Controls.Add(this.lblcontacts);
            this.Controls.Add(this.gbVisuelMessage);
            this.Controls.Add(this.pibLogo);
            this.Controls.Add(this.msDiscussion);
            this.MainMenuStrip = this.msDiscussion;
            this.Name = "FrmDiscussion";
            this.Text = "CFPTAPP";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmDiscussion_FormClosed);
            this.Load += new System.EventHandler(this.FrmDiscussion_Load);
            this.msDiscussion.ResumeLayout(false);
            this.msDiscussion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).EndInit();
            this.gbVisuelMessage.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msDiscussion;
        private System.Windows.Forms.ToolStripMenuItem tsmCompte;
        private System.Windows.Forms.ToolStripMenuItem tsmDiscussions;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.PictureBox pibLogo;
        private System.Windows.Forms.GroupBox gbVisuelMessage;
        private System.Windows.Forms.Label lblcontacts;
        private System.Windows.Forms.ListBox lsbContacts;
        private System.Windows.Forms.TextBox tbxEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Button btnAddFriend;
        private System.Windows.Forms.TextBox tbxSendMessage;
        private System.Windows.Forms.Button btnSendMessage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flpListMsg;
        private System.Windows.Forms.ToolStripMenuItem deconnexionToolStripMenuItem;
    }
}