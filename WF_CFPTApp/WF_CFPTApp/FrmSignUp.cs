﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_CFPTApp
{
    public partial class FrmSignUp : Form
    {
        // Constantes
        #region Constantes
        
        #endregion
        
        // Champs
        #region Champs
        SQLFunction _db;
        string _email;
        string _username;
        string _password;
        string _reTypePassword;
        string _errorMsg;

        #endregion

        // Propriétés
        #region Propriétés
        internal SQLFunction Db { get => _db; set => _db = value; }

        public string Email { get => _email; set => _email = value; }

        public string Username { get => _username; set => _username = value; }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = HashWithSHA256(value);
            }
        }

        public string ReTypePassword
        {
            get
            {
                return _reTypePassword;
            }
            set
            {
                _reTypePassword = HashWithSHA256(value);
            }
        }

        public string ErrorMsg { get => _errorMsg; set => _errorMsg = value; }

        #endregion

        // Constructeurs
        #region Constructeurs
        public FrmSignUp()
        {
            Db = new SQLFunction();
            Email = "";
            Username = "";
            Password = "";
            ReTypePassword = "";
            ErrorMsg = "";

            InitializeComponent();
        }

        #endregion

        // Méthodes
        #region Méthodes
        private void FrmSignUp_Load(object sender, EventArgs e)
        {
            refreshInputText();
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            Email = tbxEmail.Text;
            Username = tbxUsername.Text;
            Password = tbxPassword.Text;
            ReTypePassword = tbxRePassword.Text;
            ValidateEntre();
        }

        public void refreshInputText()
        {
            tbxEmail.Text = Email;
            tbxUsername.Text = Username;
            tbxPassword.Text = "";
            tbxRePassword.Text = "";
            lblErroMsg.Text = ErrorMsg;
        }
        
        public void ValidateEntre()
        {
            // Dans la conditions remplacer le chiffre 3 par l'index de la collum mail
            if (Password != ReTypePassword && Db.SelectAllUsers()[0].Contains(Email))
            {
                ErrorMsg = "password or username incorect";
                refreshInputText();
            }
            else
            {
                Db.InsertNesPersons(Username, Email, "", "", false);
                Db.InsertNesUsers(Email, Password, "");

                this.DialogResult = DialogResult.OK;
            }
        }

        // Encyrpte une chaine de caractères avec sha256
        public static String HashWithSHA256(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        #endregion
    }
}
