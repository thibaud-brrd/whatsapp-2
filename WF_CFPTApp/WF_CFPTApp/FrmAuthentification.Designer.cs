﻿namespace WF_CFPTApp
{
    partial class FrmAuthentification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAuthentification));
            this.msAuthentification = new System.Windows.Forms.MenuStrip();
            this.tsmCompte = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDiscussions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.pibLogo = new System.Windows.Forms.PictureBox();
            this.lblTitre = new System.Windows.Forms.Label();
            this.lblNotif = new System.Windows.Forms.Label();
            this.tbxCode = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnResend = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.msAuthentification.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // msAuthentification
            // 
            this.msAuthentification.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCompte,
            this.tsmDiscussions,
            this.tsmHelp});
            this.msAuthentification.Location = new System.Drawing.Point(0, 0);
            this.msAuthentification.Name = "msAuthentification";
            this.msAuthentification.Size = new System.Drawing.Size(1264, 24);
            this.msAuthentification.TabIndex = 0;
            this.msAuthentification.Text = "menuStrip1";
            // 
            // tsmCompte
            // 
            this.tsmCompte.Name = "tsmCompte";
            this.tsmCompte.Size = new System.Drawing.Size(62, 20);
            this.tsmCompte.Text = "Compte";
            // 
            // tsmDiscussions
            // 
            this.tsmDiscussions.Name = "tsmDiscussions";
            this.tsmDiscussions.Size = new System.Drawing.Size(80, 20);
            this.tsmDiscussions.Text = "Discussions";
            // 
            // tsmHelp
            // 
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmHelp.Text = "Help";
            // 
            // pibLogo
            // 
            this.pibLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pibLogo.Image = ((System.Drawing.Image)(resources.GetObject("pibLogo.Image")));
            this.pibLogo.Location = new System.Drawing.Point(12, 27);
            this.pibLogo.Name = "pibLogo";
            this.pibLogo.Size = new System.Drawing.Size(130, 130);
            this.pibLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pibLogo.TabIndex = 1;
            this.pibLogo.TabStop = false;
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lblTitre.Location = new System.Drawing.Point(148, 57);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(510, 55);
            this.lblTitre.TabIndex = 12;
            this.lblTitre.Text = "Double-Authentifaction";
            // 
            // lblNotif
            // 
            this.lblNotif.AutoSize = true;
            this.lblNotif.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.lblNotif.Location = new System.Drawing.Point(315, 208);
            this.lblNotif.Name = "lblNotif";
            this.lblNotif.Size = new System.Drawing.Size(594, 37);
            this.lblNotif.TabIndex = 12;
            this.lblNotif.Text = "You have received a code on your email.";
            // 
            // tbxCode
            // 
            this.tbxCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxCode.Location = new System.Drawing.Point(521, 286);
            this.tbxCode.MaxLength = 0;
            this.tbxCode.Name = "tbxCode";
            this.tbxCode.Size = new System.Drawing.Size(226, 32);
            this.tbxCode.TabIndex = 0;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.lblCode.Location = new System.Drawing.Point(445, 289);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(70, 26);
            this.lblCode.TabIndex = 12;
            this.lblCode.Text = "Code:";
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnCancel.Location = new System.Drawing.Point(401, 349);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(150, 40);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnResend
            // 
            this.btnResend.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnResend.Location = new System.Drawing.Point(557, 349);
            this.btnResend.Name = "btnResend";
            this.btnResend.Size = new System.Drawing.Size(150, 40);
            this.btnResend.TabIndex = 2;
            this.btnResend.Text = "Resend";
            this.btnResend.UseVisualStyleBackColor = true;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnConfirm.Location = new System.Drawing.Point(713, 349);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(150, 40);
            this.btnConfirm.TabIndex = 1;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // FrmAuthentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnResend);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.tbxCode);
            this.Controls.Add(this.lblNotif);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.pibLogo);
            this.Controls.Add(this.msAuthentification);
            this.MainMenuStrip = this.msAuthentification;
            this.Name = "FrmAuthentification";
            this.Text = "CFPTAPP";
            this.msAuthentification.ResumeLayout(false);
            this.msAuthentification.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msAuthentification;
        private System.Windows.Forms.ToolStripMenuItem tsmCompte;
        private System.Windows.Forms.ToolStripMenuItem tsmDiscussions;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.PictureBox pibLogo;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.Label lblNotif;
        private System.Windows.Forms.TextBox tbxCode;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnResend;
        private System.Windows.Forms.Button btnConfirm;
    }
}