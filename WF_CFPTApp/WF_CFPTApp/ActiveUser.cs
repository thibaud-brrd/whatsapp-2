﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;

namespace WF_CFPTApp
{
    class ActiveUser
    {
        // Constantes
        #region Constantes
        private static byte[] AES_KEY = { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16 };
        #endregion

        // Champs
        #region Champs
        private byte[] _key;
        private string _user;
        #endregion

        // Propriétés
        #region Propriétés
        public byte[] Key
        {
            get
            {
                return this._key;
            }
            set
            {
                this._key = value;
            }
        }
        public string User
        {
            get
            {
                return this._user;
            }
            set
            {
                this._user = value;
            }
        }

        #endregion

        // Constructeurs
        #region Constructeurs
        public ActiveUser(byte[] key, string user)
        {
            this.Key = key;
            this.User = user;
        }

        public ActiveUser()
        {
            this.Key = AES_KEY;
            this.User = String.Empty;
        }

        #endregion

        // Méthodes
        #region Méthodes
        public void Encrypt(string filename)
        {
            using (FileStream myStream = new FileStream(filename, FileMode.OpenOrCreate))
            {
                using (Aes aes = Aes.Create())
                {
                    aes.Key = this.Key;

                    byte[] iv = aes.IV;
                    myStream.Write(iv, 0, iv.Length);

                    using (CryptoStream cryptStream = new CryptoStream(myStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        using (StreamWriter sWriter = new StreamWriter(cryptStream))
                        {
                            sWriter.WriteLine(this.User);
                        }
                    }
                }
            }
        }
        public void Decrypt(string filename)
        {
            using (FileStream myStream = new FileStream(filename, FileMode.Open))
            {
                using (Aes aes = Aes.Create())
                {
                    byte[] iv = new byte[aes.IV.Length];
                    myStream.Read(iv, 0, iv.Length);

                    using (CryptoStream cryptStream = new CryptoStream(myStream, aes.CreateDecryptor(this.Key, iv), CryptoStreamMode.Read))
                    {
                        using (StreamReader sReader = new StreamReader(cryptStream))
                        {
                            this.User = sReader.ReadToEnd();
                        }
                    }
                }
            }
        }
        #endregion
    }
}
