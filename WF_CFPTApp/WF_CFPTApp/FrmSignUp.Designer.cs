﻿namespace WF_CFPTApp
{
    partial class FrmSignUp
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.msSignUp = new System.Windows.Forms.MenuStrip();
            this.tsmCompte = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmDiscussions = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.pibLogo = new System.Windows.Forms.PictureBox();
            this.lblSignUp = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblPwd = new System.Windows.Forms.Label();
            this.lblRePwd = new System.Windows.Forms.Label();
            this.tbxUsername = new System.Windows.Forms.TextBox();
            this.tbxEmail = new System.Windows.Forms.TextBox();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.tbxRePassword = new System.Windows.Forms.TextBox();
            this.btnAddImg = new System.Windows.Forms.Button();
            this.lblAddProfileImage = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSignUp = new System.Windows.Forms.Button();
            this.lblErroMsg = new System.Windows.Forms.Label();
            this.btnSignIn = new System.Windows.Forms.Button();
            this.msSignUp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // msSignUp
            // 
            this.msSignUp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmCompte,
            this.tsmDiscussions,
            this.tsmHelp});
            this.msSignUp.Location = new System.Drawing.Point(0, 0);
            this.msSignUp.Name = "msSignUp";
            this.msSignUp.Size = new System.Drawing.Size(1264, 24);
            this.msSignUp.TabIndex = 0;
            this.msSignUp.Text = "menuStrip1";
            // 
            // tsmCompte
            // 
            this.tsmCompte.Name = "tsmCompte";
            this.tsmCompte.Size = new System.Drawing.Size(62, 20);
            this.tsmCompte.Text = "Compte";
            // 
            // tsmDiscussions
            // 
            this.tsmDiscussions.Name = "tsmDiscussions";
            this.tsmDiscussions.Size = new System.Drawing.Size(80, 20);
            this.tsmDiscussions.Text = "Discussions";
            // 
            // tsmHelp
            // 
            this.tsmHelp.Name = "tsmHelp";
            this.tsmHelp.Size = new System.Drawing.Size(44, 20);
            this.tsmHelp.Text = "Help";
            // 
            // pibLogo
            // 
            this.pibLogo.Image = global::WF_CFPTApp.Properties.Resources.logo2;
            this.pibLogo.Location = new System.Drawing.Point(12, 27);
            this.pibLogo.Name = "pibLogo";
            this.pibLogo.Size = new System.Drawing.Size(130, 130);
            this.pibLogo.TabIndex = 1;
            this.pibLogo.TabStop = false;
            // 
            // lblSignUp
            // 
            this.lblSignUp.AutoSize = true;
            this.lblSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignUp.Location = new System.Drawing.Point(148, 62);
            this.lblSignUp.Name = "lblSignUp";
            this.lblSignUp.Size = new System.Drawing.Size(195, 55);
            this.lblSignUp.TabIndex = 15;
            this.lblSignUp.Text = "Sign Up";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(381, 250);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(182, 37);
            this.lblUsername.TabIndex = 15;
            this.lblUsername.Text = "Username :";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.Location = new System.Drawing.Point(448, 321);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(115, 37);
            this.lblEmail.TabIndex = 15;
            this.lblEmail.Text = "Email :";
            // 
            // lblPwd
            // 
            this.lblPwd.AutoSize = true;
            this.lblPwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPwd.Location = new System.Drawing.Point(387, 395);
            this.lblPwd.Name = "lblPwd";
            this.lblPwd.Size = new System.Drawing.Size(176, 37);
            this.lblPwd.TabIndex = 15;
            this.lblPwd.Text = "Password :";
            // 
            // lblRePwd
            // 
            this.lblRePwd.AutoSize = true;
            this.lblRePwd.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRePwd.Location = new System.Drawing.Point(273, 472);
            this.lblRePwd.Name = "lblRePwd";
            this.lblRePwd.Size = new System.Drawing.Size(290, 37);
            this.lblRePwd.TabIndex = 15;
            this.lblRePwd.Text = "Re-type password :";
            // 
            // tbxUsername
            // 
            this.tbxUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxUsername.Location = new System.Drawing.Point(578, 255);
            this.tbxUsername.Name = "tbxUsername";
            this.tbxUsername.Size = new System.Drawing.Size(226, 32);
            this.tbxUsername.TabIndex = 1;
            this.tbxUsername.Text = " ";
            // 
            // tbxEmail
            // 
            this.tbxEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxEmail.Location = new System.Drawing.Point(578, 326);
            this.tbxEmail.Name = "tbxEmail";
            this.tbxEmail.Size = new System.Drawing.Size(226, 32);
            this.tbxEmail.TabIndex = 2;
            this.tbxEmail.Text = " ";
            // 
            // tbxPassword
            // 
            this.tbxPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxPassword.Location = new System.Drawing.Point(578, 400);
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.PasswordChar = '*';
            this.tbxPassword.Size = new System.Drawing.Size(226, 32);
            this.tbxPassword.TabIndex = 3;
            this.tbxPassword.Text = " ";
            // 
            // tbxRePassword
            // 
            this.tbxRePassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.tbxRePassword.Location = new System.Drawing.Point(578, 477);
            this.tbxRePassword.Name = "tbxRePassword";
            this.tbxRePassword.PasswordChar = '*';
            this.tbxRePassword.Size = new System.Drawing.Size(226, 32);
            this.tbxRePassword.TabIndex = 4;
            this.tbxRePassword.Text = " ";
            // 
            // btnAddImg
            // 
            this.btnAddImg.Font = new System.Drawing.Font("Stencil", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddImg.Location = new System.Drawing.Point(613, 111);
            this.btnAddImg.Name = "btnAddImg";
            this.btnAddImg.Size = new System.Drawing.Size(121, 113);
            this.btnAddImg.TabIndex = 0;
            this.btnAddImg.Text = "+";
            this.btnAddImg.UseVisualStyleBackColor = true;
            // 
            // lblAddProfileImage
            // 
            this.lblAddProfileImage.AutoSize = true;
            this.lblAddProfileImage.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddProfileImage.Location = new System.Drawing.Point(278, 159);
            this.lblAddProfileImage.Name = "lblAddProfileImage";
            this.lblAddProfileImage.Size = new System.Drawing.Size(285, 37);
            this.lblAddProfileImage.TabIndex = 15;
            this.lblAddProfileImage.Text = "Add profile image :";
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DarkRed;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(588, 564);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(105, 40);
            this.btnCancel.TabIndex = 6;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnSignUp
            // 
            this.btnSignUp.BackColor = System.Drawing.Color.DarkRed;
            this.btnSignUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.5F);
            this.btnSignUp.ForeColor = System.Drawing.Color.White;
            this.btnSignUp.Location = new System.Drawing.Point(699, 564);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(105, 40);
            this.btnSignUp.TabIndex = 5;
            this.btnSignUp.Text = "Sign Up";
            this.btnSignUp.UseVisualStyleBackColor = false;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // lblErroMsg
            // 
            this.lblErroMsg.AutoSize = true;
            this.lblErroMsg.BackColor = System.Drawing.Color.Red;
            this.lblErroMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErroMsg.Location = new System.Drawing.Point(593, 60);
            this.lblErroMsg.Name = "lblErroMsg";
            this.lblErroMsg.Size = new System.Drawing.Size(131, 29);
            this.lblErroMsg.TabIndex = 16;
            this.lblErroMsg.Text = "lblErroMsg";
            // 
            // btnSignIn
            // 
            this.btnSignIn.DialogResult = System.Windows.Forms.DialogResult.Abort;
            this.btnSignIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignIn.ForeColor = System.Drawing.SystemColors.Highlight;
            this.btnSignIn.Location = new System.Drawing.Point(578, 526);
            this.btnSignIn.Name = "btnSignIn";
            this.btnSignIn.Size = new System.Drawing.Size(226, 23);
            this.btnSignIn.TabIndex = 17;
            this.btnSignIn.Text = "You already have an account ?";
            this.btnSignIn.UseVisualStyleBackColor = true;
            // 
            // FrmSignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.btnSignIn);
            this.Controls.Add(this.lblErroMsg);
            this.Controls.Add(this.btnSignUp);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblAddProfileImage);
            this.Controls.Add(this.btnAddImg);
            this.Controls.Add(this.tbxRePassword);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.tbxEmail);
            this.Controls.Add(this.tbxUsername);
            this.Controls.Add(this.lblRePwd);
            this.Controls.Add(this.lblPwd);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblSignUp);
            this.Controls.Add(this.pibLogo);
            this.Controls.Add(this.msSignUp);
            this.MainMenuStrip = this.msSignUp;
            this.Name = "FrmSignUp";
            this.Text = "CFPTAPP";
            this.Load += new System.EventHandler(this.FrmSignUp_Load);
            this.msSignUp.ResumeLayout(false);
            this.msSignUp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pibLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msSignUp;
        private System.Windows.Forms.ToolStripMenuItem tsmCompte;
        private System.Windows.Forms.ToolStripMenuItem tsmDiscussions;
        private System.Windows.Forms.ToolStripMenuItem tsmHelp;
        private System.Windows.Forms.PictureBox pibLogo;
        private System.Windows.Forms.Label lblSignUp;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblPwd;
        private System.Windows.Forms.Label lblRePwd;
        private System.Windows.Forms.TextBox tbxUsername;
        private System.Windows.Forms.TextBox tbxEmail;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.TextBox tbxRePassword;
        private System.Windows.Forms.Button btnAddImg;
        private System.Windows.Forms.Label lblAddProfileImage;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSignUp;
        private System.Windows.Forms.Label lblErroMsg;
        private System.Windows.Forms.Button btnSignIn;
    }
}

