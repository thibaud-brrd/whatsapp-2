﻿/*
 * Processor with methods
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace WFTestAPI
{
    class MessageProcessor
    {
        private static string url = "messages"; // Routes name

        public static async Task<List<Message>> LoadMessage()
        {
            List<Message> message = await ApiHelper.GetAsync<List<Message>>(url);

            return message;
        }
        
        public static async Task PostMessage(Message message)
        {
            await ApiHelper.PostAsync(message, url);
        }

        public static async Task DeleteMessage(string id)
        {
            await ApiHelper.DeleteAsync(url + "/" + id);
        }

        public static async Task UpdateMessage(Message message, string id)
        {
            await ApiHelper.PutAsync<Message>(message, url + "/" + id);
        }
    }

}
