﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFTestAPI
{
    public partial class Form1 : Form
    {
        List<Message> messageList = new List<Message>();
        public Form1()
        {
            InitializeComponent();
            ApiHelper.InitializeClient();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbxId.Text = messageList[lsbMessage.SelectedIndex].id.ToString();
            tbxFrom.Text = messageList[lsbMessage.SelectedIndex].senderId.ToString();
            tbxTo.Text = messageList[lsbMessage.SelectedIndex].receiverId.ToString();
            tbxContent.Text = messageList[lsbMessage.SelectedIndex].content.ToString();
        }

        private async void btnRefresh_Click(object sender, EventArgs e)
        {
            await LoadMessages();
        }

        private async Task PostMessage()
        {
           await MessageProcessor.PostMessage(new Message { senderId = tbxFrom.Text, receiverId = tbxTo.Text, content = tbxContent.Text });
           await LoadMessages();
        }

        private async Task LoadMessages()
        {
            var message = await MessageProcessor.LoadMessage();

            //var uriSource = new Uri(message., UriKind.Absolute);

            // Vider la liste temporaire et rajoutez le contenu
            messageList.Clear();
            messageList.AddRange(message);

            // Videz ensuite la ListBox pour y ajoutez le contenu de la liste temporaire
            lsbMessage.Items.Clear();
            lsbMessage.Items.AddRange(messageList.ToArray<Message>());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void button2_Click(object sender, EventArgs e)
        {
            await DeleteMessage(lsbMessage.SelectedIndex);
        }

        private async Task DeleteMessage(int id)
        {
            await MessageProcessor.DeleteMessage(tbxId.Text);
            await LoadMessages();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            await LoadMessages();
            btnAdd.Enabled = false;
            btnDelete.Enabled = false;
            btnUpdate.Enabled = false;
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            await PostMessage();
        }

        private void tbxContent_TextChanged(object sender, EventArgs e)
        {

            if (tbxContent.Text != "" && tbxId.Text != "")
            {
                btnUpdate.Enabled = true;
            }
            else if (tbxContent.Text != "")
            {
                btnAdd.Enabled = true;
                btnUpdate.Enabled = false;
            }
            else if (tbxId.Text != "")
            {
                btnDelete.Enabled = true;
                btnUpdate.Enabled = false;
            }
            else
            {
                btnDelete.Enabled = false;
                btnAdd.Enabled = false;
                btnUpdate.Enabled = false;
            }
        }

        private async void btnUpdate_Click(object sender, EventArgs e)
        {
            await MessageProcessor.UpdateMessage(new Message { content = tbxContent.Text }, tbxId.Text);
            await LoadMessages();
        }
    }
}
