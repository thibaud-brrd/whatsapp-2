﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF_cfpta_design
{
    public class DisplayUser
    {
        // Champs
        private string _email;
        private string _username;
        private Image _profile;

        // Propriétées
        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                this._email = value.Trim();
                if (!this._email.Contains("@"))
                {
                    this._email = "";
                }
            }
        }

        public string Username
        {
            get
            {
                return this._username;
            }
            set
            {
                this._username = value;
            }
        }

        public Image Profile
        {
            get
            {
                return this._profile;
            }
            set
            {
                this._profile = value;
            }
        }
        // Constructeur
        public DisplayUser(string email, string username, Image profile)
        {
            this.Email = email;
            this.Username = username;
            this.Profile = profile;
        }
    }
}
