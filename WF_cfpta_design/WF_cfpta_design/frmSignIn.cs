﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_cfpta_design
{
    public partial class frmSignIn : Form
    {
        // Constantes
        #region Constantes

        #endregion

        // Champs
        #region Champs
        SQLFunction _db;
        string _email;
        string _password;
        string _errorMsg;
        frmSignUp frm2 = new frmSignUp();

        #endregion

        // Porpriétés
        #region Propriétés
        internal SQLFunction Db { get => _db; set => _db = value; }

        public string Email { get => _email; set => _email = value; }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = HashWithSHA256(value);
            }
        }

        public string ErrorMsg { get => _errorMsg; set => _errorMsg = value; }

        #endregion

        // Constructeurs
        #region Constructeurs
        public frmSignIn()
        {
            Db = new SQLFunction();
            Email = "";
            Password = "";
            ErrorMsg = "";

            InitializeComponent();
        }

        #endregion


        // Méthodes
        #region Méthodes
        private void frmSignIn_Load(object sender, EventArgs e)
        {
            refreshInputText();
        }


        private void tbxPassword_Click(object sender, EventArgs e)
        {
            tbxPassword.Clear();
            tbxPassword.PasswordChar = '*';

            pbPassword.BackgroundImage = Properties.Resources.icons8_lock_24_bleu;
            panel2.BackColor = Color.FromArgb(78, 184, 206);
            tbxPassword.ForeColor = Color.FromArgb(78, 184, 206);

            pbMail.BackgroundImage = Properties.Resources.icons8_important_mail_24;
            panel3.BackColor = Color.WhiteSmoke;
            tbxMail.ForeColor = Color.WhiteSmoke;
        }

        private void tbxMail_Click(object sender, EventArgs e)
        {
            tbxMail.Clear();

            pbPassword.BackgroundImage = Properties.Resources.icons8_lock_24;
            panel2.BackColor = Color.WhiteSmoke;
            tbxPassword.ForeColor = Color.WhiteSmoke;

            pbMail.BackgroundImage = Properties.Resources.icons8_important_mail_24_bleu;
            panel3.BackColor = Color.FromArgb(78, 184, 206);
            tbxMail.ForeColor = Color.FromArgb(78, 184, 206);
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            tm1.Start();
            frm2.Show();
        }

        private void tm1_Tick(object sender, EventArgs e)
        {
            frm2.Left += 10;
            if (frm2.Left >= 1130)
            {
                tm1.Stop();
                this.TopMost = false;
                frm2.TopMost = false;
                tm2.Start();
            }
        }

        private void tm2_Tick(object sender, EventArgs e)
        {
            frm2.Left -= 10;
            if (frm2.Left <= 825)
            {
                tm2.Stop();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSignIn_Click(object sender, EventArgs e)
        {
            Email = tbxMail.Text;
            Password = tbxPassword.Text;
            ValidateEntre();
        }

        public void refreshInputText()
        {
            tbxMail.Text = Email;
            tbxPassword.Text = "";
            // lblError.Text = ErrorMsg;
        }

        public void ValidateEntre()
        {
            if (Db.SelectAllUsers()[1].Contains(Email) && Password == Db.lireUtilisateur(Email).password.ToString())
            {
               // lblError.Text = "successful connection";
                this.DialogResult = DialogResult.OK;
            }
            else
            {
               // ErrorMsg = "password or username incorect";
                refreshInputText();
            }
        }

        // Encyrpte une chaine de caractères avec sha256
        public static String HashWithSHA256(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        #endregion
    }
}
