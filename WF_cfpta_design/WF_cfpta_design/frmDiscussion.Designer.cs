﻿namespace WF_cfpta_design
{
    partial class frmDiscussion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDiscussion));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.compteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.déconexionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.discussionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblClose = new System.Windows.Forms.Label();
            this.gbFriend = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panelAddFriend = new System.Windows.Forms.Panel();
            this.tbxAddFriend = new System.Windows.Forms.TextBox();
            this.pbAddFriend = new System.Windows.Forms.PictureBox();
            this.flpListFriend = new System.Windows.Forms.FlowLayoutPanel();
            this.lblTextContact = new System.Windows.Forms.Label();
            this.gbDetailFriend = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pbSendMessage = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbxSendMessage = new System.Windows.Forms.TextBox();
            this.gbVisuelMessage = new System.Windows.Forms.GroupBox();
            this.flpListMsg = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            this.gbFriend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddFriend)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSendMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gbVisuelMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compteToolStripMenuItem,
            this.discussionsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1280, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // compteToolStripMenuItem
            // 
            this.compteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.déconexionToolStripMenuItem});
            this.compteToolStripMenuItem.Name = "compteToolStripMenuItem";
            this.compteToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.compteToolStripMenuItem.Text = "Compte";
            // 
            // déconexionToolStripMenuItem
            // 
            this.déconexionToolStripMenuItem.Name = "déconexionToolStripMenuItem";
            this.déconexionToolStripMenuItem.Size = new System.Drawing.Size(137, 22);
            this.déconexionToolStripMenuItem.Text = "Déconexion";
            this.déconexionToolStripMenuItem.Click += new System.EventHandler(this.déconexionToolStripMenuItem_Click);
            // 
            // discussionsToolStripMenuItem
            // 
            this.discussionsToolStripMenuItem.Name = "discussionsToolStripMenuItem";
            this.discussionsToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.discussionsToolStripMenuItem.Text = "Discussions";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // lblClose
            // 
            this.lblClose.AutoSize = true;
            this.lblClose.BackColor = System.Drawing.Color.White;
            this.lblClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblClose.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblClose.Location = new System.Drawing.Point(1259, 3);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(19, 18);
            this.lblClose.TabIndex = 1;
            this.lblClose.Text = "X";
            this.lblClose.Click += new System.EventHandler(this.label1_Click);
            // 
            // gbFriend
            // 
            this.gbFriend.Controls.Add(this.pictureBox3);
            this.gbFriend.Controls.Add(this.panelAddFriend);
            this.gbFriend.Controls.Add(this.tbxAddFriend);
            this.gbFriend.Controls.Add(this.pbAddFriend);
            this.gbFriend.Controls.Add(this.flpListFriend);
            this.gbFriend.Controls.Add(this.lblTextContact);
            this.gbFriend.ForeColor = System.Drawing.Color.White;
            this.gbFriend.Location = new System.Drawing.Point(12, 29);
            this.gbFriend.Name = "gbFriend";
            this.gbFriend.Size = new System.Drawing.Size(315, 679);
            this.gbFriend.TabIndex = 3;
            this.gbFriend.TabStop = false;
            this.gbFriend.Text = "Friend List";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.Location = new System.Drawing.Point(25, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 55);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // panelAddFriend
            // 
            this.panelAddFriend.BackColor = System.Drawing.Color.White;
            this.panelAddFriend.Location = new System.Drawing.Point(6, 663);
            this.panelAddFriend.Name = "panelAddFriend";
            this.panelAddFriend.Size = new System.Drawing.Size(293, 1);
            this.panelAddFriend.TabIndex = 5;
            // 
            // tbxAddFriend
            // 
            this.tbxAddFriend.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxAddFriend.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.tbxAddFriend.Location = new System.Drawing.Point(6, 625);
            this.tbxAddFriend.Multiline = true;
            this.tbxAddFriend.Name = "tbxAddFriend";
            this.tbxAddFriend.Size = new System.Drawing.Size(247, 35);
            this.tbxAddFriend.TabIndex = 4;
            this.tbxAddFriend.TabStop = false;
            this.tbxAddFriend.Text = "Add Firend";
            this.tbxAddFriend.Click += new System.EventHandler(this.tbxAddFriend_Click);
            // 
            // pbAddFriend
            // 
            this.pbAddFriend.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_add_user_group_man_man_40__1_;
            this.pbAddFriend.Location = new System.Drawing.Point(259, 622);
            this.pbAddFriend.Name = "pbAddFriend";
            this.pbAddFriend.Size = new System.Drawing.Size(40, 40);
            this.pbAddFriend.TabIndex = 3;
            this.pbAddFriend.TabStop = false;
            // 
            // flpListFriend
            // 
            this.flpListFriend.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpListFriend.Location = new System.Drawing.Point(6, 82);
            this.flpListFriend.Name = "flpListFriend";
            this.flpListFriend.Size = new System.Drawing.Size(303, 530);
            this.flpListFriend.TabIndex = 2;
            // 
            // lblTextContact
            // 
            this.lblTextContact.AutoSize = true;
            this.lblTextContact.Font = new System.Drawing.Font("Arial Narrow", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextContact.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.lblTextContact.Location = new System.Drawing.Point(97, 16);
            this.lblTextContact.Name = "lblTextContact";
            this.lblTextContact.Size = new System.Drawing.Size(212, 63);
            this.lblTextContact.TabIndex = 1;
            this.lblTextContact.Text = "Contacts";
            this.lblTextContact.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbDetailFriend
            // 
            this.gbDetailFriend.ForeColor = System.Drawing.Color.White;
            this.gbDetailFriend.Location = new System.Drawing.Point(1123, 170);
            this.gbDetailFriend.Name = "gbDetailFriend";
            this.gbDetailFriend.Size = new System.Drawing.Size(148, 359);
            this.gbDetailFriend.TabIndex = 5;
            this.gbDetailFriend.TabStop = false;
            this.gbDetailFriend.Text = "Détaille";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pbSendMessage);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.tbxSendMessage);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(333, 642);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(782, 66);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Send Message";
            // 
            // pbSendMessage
            // 
            this.pbSendMessage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbSendMessage.BackgroundImage")));
            this.pbSendMessage.Location = new System.Drawing.Point(741, 17);
            this.pbSendMessage.Name = "pbSendMessage";
            this.pbSendMessage.Size = new System.Drawing.Size(35, 35);
            this.pbSendMessage.TabIndex = 3;
            this.pbSendMessage.TabStop = false;
            this.pbSendMessage.Click += new System.EventHandler(this.pbSendMessage_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::WF_cfpta_design.Properties.Resources.icons8_trombone_24__1_;
            this.pictureBox2.Location = new System.Drawing.Point(56, 20);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 28);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tbxSendMessage
            // 
            this.tbxSendMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxSendMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxSendMessage.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSendMessage.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.tbxSendMessage.Location = new System.Drawing.Point(86, 19);
            this.tbxSendMessage.Multiline = true;
            this.tbxSendMessage.Name = "tbxSendMessage";
            this.tbxSendMessage.Size = new System.Drawing.Size(650, 28);
            this.tbxSendMessage.TabIndex = 0;
            this.tbxSendMessage.TabStop = false;
            this.tbxSendMessage.Text = "Taper un message";
            // 
            // gbVisuelMessage
            // 
            this.gbVisuelMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(40)))), ((int)(((byte)(49)))));
            this.gbVisuelMessage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.gbVisuelMessage.Controls.Add(this.flpListMsg);
            this.gbVisuelMessage.Controls.Add(this.groupBox2);
            this.gbVisuelMessage.ForeColor = System.Drawing.Color.White;
            this.gbVisuelMessage.Location = new System.Drawing.Point(333, 33);
            this.gbVisuelMessage.Name = "gbVisuelMessage";
            this.gbVisuelMessage.Size = new System.Drawing.Size(782, 603);
            this.gbVisuelMessage.TabIndex = 4;
            this.gbVisuelMessage.TabStop = false;
            this.gbVisuelMessage.Text = "Name Friend";
            // 
            // flpListMsg
            // 
            this.flpListMsg.Location = new System.Drawing.Point(6, 17);
            this.flpListMsg.Name = "flpListMsg";
            this.flpListMsg.Size = new System.Drawing.Size(770, 580);
            this.flpListMsg.TabIndex = 6;
            // 
            // groupBox2
            // 
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(801, 139);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 536);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Friend List";
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbLogo.BackgroundImage")));
            this.pbLogo.Location = new System.Drawing.Point(1134, 43);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(121, 121);
            this.pbLogo.TabIndex = 2;
            this.pbLogo.TabStop = false;
            // 
            // frmDiscussion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(1280, 720);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbDetailFriend);
            this.Controls.Add(this.gbVisuelMessage);
            this.Controls.Add(this.gbFriend);
            this.Controls.Add(this.pbLogo);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmDiscussion";
            this.Text = "frmDiscussion";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmDiscussion_FormClosed);
            this.Load += new System.EventHandler(this.frmDiscussion_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.gbFriend.ResumeLayout(false);
            this.gbFriend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAddFriend)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSendMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gbVisuelMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem compteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem déconexionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem discussionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.GroupBox gbFriend;
        private System.Windows.Forms.Label lblTextContact;
        private System.Windows.Forms.GroupBox gbVisuelMessage;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox gbDetailFriend;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pbSendMessage;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox tbxSendMessage;
        private System.Windows.Forms.TextBox tbxAddFriend;
        private System.Windows.Forms.PictureBox pbAddFriend;
        private System.Windows.Forms.FlowLayoutPanel flpListFriend;
        private System.Windows.Forms.Panel panelAddFriend;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.FlowLayoutPanel flpListMsg;
    }
}