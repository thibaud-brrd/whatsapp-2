﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WF_cfpta_design
{
    public partial class frmDiscussion : Form
    {

        // Constantes
        #region Constantes
        const string FILE_OF_ACTIVEUSER = "activeUser.txt";

        #endregion


        // Champs
        #region Champs
        ActiveUser _aUser;
        DisplayUser _dUser;
        List<Message> _messageList = new List<Message>();

        #endregion

        // Propriétés
        #region Propriétés
        internal ActiveUser AUser { get => _aUser; set => _aUser = value; }

        internal DisplayUser DUser { get => _dUser; set => _dUser = value; }

        internal List<Message> MessageList { get => _messageList; set => _messageList = value; }

        #endregion

        // Constructeurs
        #region Constructeurs
        public frmDiscussion()
        {
            AUser = new ActiveUser();

            ApiHelper.InitializeClient();

            InitializeComponent();
        }

        #endregion


        // Méthodes
        #region Méthodes
        private void label1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void tbxAddFriend_Click(object sender, EventArgs e)
        {
            tbxAddFriend.Clear();
            pbAddFriend.BackgroundImage = Properties.Resources.icons8_add_user_group_man_man_40;
            panelAddFriend.BackColor = Color.FromArgb(78, 184, 206);
            tbxAddFriend.ForeColor = Color.FromArgb(78, 184, 206);
        }

        private async void frmDiscussion_Load(object sender, EventArgs e)
        {
            activeUserLoader();
            if (AUser.User == String.Empty)
            {
                DialogResult resultSelect;
                frmSelectAccount dialogueSelect = new frmSelectAccount();
                this.Hide();
                resultSelect = dialogueSelect.ShowDialog();
                if (resultSelect == DialogResult.OK)
                {
                    DUser = dialogueSelect.getSelectedUser();
                    this.Text = "CFPTApp - " + DUser.Username;
                    await LoadMessages();
                }
                else
                {
                    Close();
                }
            }
            else
            {
                DUser = new DisplayUser(AUser.User, "get username from DB", Properties.Resources.icons8_customer_24);
                this.Text = "CFPTApp - " + DUser.Username;
                await LoadMessages();
            }
        }

        private async void pbSendMessage_Click(object sender, EventArgs e)
        {
            await PostMessage();
        }

        private async Task LoadMessages()
        {
            var message = await MessageProcessor.LoadMessage();

            // Vider la liste temporaire et rajoutez le contenu
            MessageList.Clear();
            MessageList.AddRange(message);

            flpListMsg.Controls.Clear();
            foreach (Message m in MessageList)
            {
                createDynamiqueMessage(m.id, m.content, m.senderId == DUser.Email ? true : false);
            }
        }

        private async Task PostMessage()
        {
            if (DUser.Email == "brian.grn@eduge.ch")
            {
                await MessageProcessor.PostMessage(new Message { senderId = DUser.Email, receiverId = "darius.gmsds@eduge.ch", content = tbxSendMessage.Text });
            }
            else
            {
                await MessageProcessor.PostMessage(new Message { senderId = DUser.Email, receiverId = "brian.grn@eduge.ch", content = tbxSendMessage.Text });
            }

            await LoadMessages();
        }

        private void createDynamiqueMessage(int id, string content, bool isYours)
        {
            Label lblMessage = new System.Windows.Forms.Label();
            GroupBox grbMessage = new System.Windows.Forms.GroupBox();

            lblMessage.AutoSize = true;
            if (isYours)
            {
                lblMessage.Dock = System.Windows.Forms.DockStyle.Right;
            }
            else
            {
                lblMessage.Dock = System.Windows.Forms.DockStyle.Left;
            }
            lblMessage.Name = "lblMessage" + id.ToString();
            lblMessage.Size = new System.Drawing.Size(35, 27);
            lblMessage.TabIndex = 0;
            lblMessage.Text = content;

            grbMessage.Controls.Add(lblMessage);
            grbMessage.Location = new System.Drawing.Point(3, 3);
            grbMessage.Name = "grbMessage" + id.ToString();
            grbMessage.Size = new System.Drawing.Size(980, 37);
            grbMessage.TabIndex = 0;
            grbMessage.TabStop = false;

            this.flpListMsg.Controls.Add(grbMessage);
        }

        private void frmDiscussion_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (DUser != null)
            {
                activeUserSaver(DUser.Email);
            }
        }

        // Fonctions d'encryption et de decryption de l'utilisateur actif
        private void activeUserLoader()
        {
            if (File.Exists(FILE_OF_ACTIVEUSER))
            {
                // Récupération de l'activeUser
                AUser.Decrypt(FILE_OF_ACTIVEUSER);
            }
            else
            {
                // Aucun fichier = activeUser
                AUser.User = String.Empty;
            }
        }

        private void activeUserSaver(string user)
        {
            AUser.User = user;
            AUser.Encrypt(FILE_OF_ACTIVEUSER);
        }

        private async void déconexionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult resultSelect;
            frmSelectAccount dialogueSelect = new frmSelectAccount();
            this.Hide();
            resultSelect = dialogueSelect.ShowDialog();
            if (resultSelect == DialogResult.OK)
            {
                DUser = dialogueSelect.getSelectedUser();
                this.Show();
                await LoadMessages();
                this.Text = "CFPTApp - " + DUser.Username;
            }
            else
            {
                Close();
            }

            if (File.Exists(FILE_OF_ACTIVEUSER))
            {
                File.Delete(FILE_OF_ACTIVEUSER);
            }
        }

#endregion
    }
}
