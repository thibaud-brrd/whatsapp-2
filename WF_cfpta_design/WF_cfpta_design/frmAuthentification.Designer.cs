﻿namespace WF_cfpta_design
{
    partial class frmAuthentification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAuthentification));
            this.lblClose = new System.Windows.Forms.Label();
            this.lblTitre = new System.Windows.Forms.Label();
            this.tbxCode = new System.Windows.Forms.TextBox();
            this.panelSeparator = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lblTextCode = new System.Windows.Forms.Label();
            this.pbCode = new System.Windows.Forms.PictureBox();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.btnResend = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panelUnderLineTbxCode = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // lblClose
            // 
            this.lblClose.AutoSize = true;
            this.lblClose.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClose.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblClose.Location = new System.Drawing.Point(274, 6);
            this.lblClose.Name = "lblClose";
            this.lblClose.Size = new System.Drawing.Size(17, 17);
            this.lblClose.TabIndex = 2;
            this.lblClose.Text = "X";
            this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
            // 
            // lblTitre
            // 
            this.lblTitre.AutoSize = true;
            this.lblTitre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTitre.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblTitre.Location = new System.Drawing.Point(5, 143);
            this.lblTitre.Name = "lblTitre";
            this.lblTitre.Size = new System.Drawing.Size(292, 34);
            this.lblTitre.TabIndex = 3;
            this.lblTitre.Text = "Double-Authentifaction";
            // 
            // tbxCode
            // 
            this.tbxCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.tbxCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxCode.ForeColor = System.Drawing.SystemColors.Window;
            this.tbxCode.Location = new System.Drawing.Point(118, 242);
            this.tbxCode.Name = "tbxCode";
            this.tbxCode.Size = new System.Drawing.Size(139, 13);
            this.tbxCode.TabIndex = 4;
            this.tbxCode.TabStop = false;
            this.tbxCode.Text = "XXXXX-XXXXX";
            this.tbxCode.Click += new System.EventHandler(this.tbxCode_Click);
            // 
            // panelSeparator
            // 
            this.panelSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(46)))), ((int)(((byte)(159)))));
            this.panelSeparator.Location = new System.Drawing.Point(-2, 139);
            this.panelSeparator.Name = "panelSeparator";
            this.panelSeparator.Size = new System.Drawing.Size(302, 1);
            this.panelSeparator.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(40, 200);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(219, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "You have received a code on your email.";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.btnConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirm.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnConfirm.Location = new System.Drawing.Point(29, 283);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(242, 41);
            this.btnConfirm.TabIndex = 7;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // lblTextCode
            // 
            this.lblTextCode.AutoSize = true;
            this.lblTextCode.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblTextCode.Location = new System.Drawing.Point(76, 241);
            this.lblTextCode.Name = "lblTextCode";
            this.lblTextCode.Size = new System.Drawing.Size(38, 13);
            this.lblTextCode.TabIndex = 10;
            this.lblTextCode.Text = "Code :";
            // 
            // pbCode
            // 
            this.pbCode.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_code_24;
            this.pbCode.Location = new System.Drawing.Point(43, 234);
            this.pbCode.Name = "pbCode";
            this.pbCode.Size = new System.Drawing.Size(24, 24);
            this.pbCode.TabIndex = 11;
            this.pbCode.TabStop = false;
            // 
            // pbLogo
            // 
            this.pbLogo.BackgroundImage = global::WF_cfpta_design.Properties.Resources.logo2;
            this.pbLogo.Location = new System.Drawing.Point(84, 12);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(121, 121);
            this.pbLogo.TabIndex = 1;
            this.pbLogo.TabStop = false;
            // 
            // btnResend
            // 
            this.btnResend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnResend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResend.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResend.ForeColor = System.Drawing.Color.White;
            this.btnResend.Location = new System.Drawing.Point(29, 330);
            this.btnResend.Name = "btnResend";
            this.btnResend.Size = new System.Drawing.Size(242, 41);
            this.btnResend.TabIndex = 12;
            this.btnResend.Text = "Resend";
            this.btnResend.UseVisualStyleBackColor = false;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(29, 377);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(242, 41);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // panelUnderLineTbxCode
            // 
            this.panelUnderLineTbxCode.BackColor = System.Drawing.Color.White;
            this.panelUnderLineTbxCode.Location = new System.Drawing.Point(43, 264);
            this.panelUnderLineTbxCode.Name = "panelUnderLineTbxCode";
            this.panelUnderLineTbxCode.Size = new System.Drawing.Size(230, 1);
            this.panelUnderLineTbxCode.TabIndex = 14;
            // 
            // frmAuthentification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(300, 440);
            this.Controls.Add(this.panelUnderLineTbxCode);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnResend);
            this.Controls.Add(this.pbCode);
            this.Controls.Add(this.lblTextCode);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelSeparator);
            this.Controls.Add(this.tbxCode);
            this.Controls.Add(this.lblTitre);
            this.Controls.Add(this.lblClose);
            this.Controls.Add(this.pbLogo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmAuthentification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CFPTAPP";
            ((System.ComponentModel.ISupportInitialize)(this.pbCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.Label lblTitre;
        private System.Windows.Forms.TextBox tbxCode;
        private System.Windows.Forms.Panel panelSeparator;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Label lblTextCode;
        private System.Windows.Forms.PictureBox pbCode;
        private System.Windows.Forms.Button btnResend;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panelUnderLineTbxCode;
    }
}