﻿namespace WF_cfpta_design
{
    partial class frmSignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSignUp));
            this.btnSignUp = new System.Windows.Forms.Button();
            this.btnGoSignIn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tbxMail = new System.Windows.Forms.TextBox();
            this.pbMail = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tbxRePassword = new System.Windows.Forms.TextBox();
            this.pbRePassword = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbxUsrname = new System.Windows.Forms.TextBox();
            this.pbUsrname = new System.Windows.Forms.PictureBox();
            this.pbIcone = new System.Windows.Forms.PictureBox();
            this.btnBackSelectAcount = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tbxPassword = new System.Windows.Forms.TextBox();
            this.pbPassword = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pbMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsrname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSignUp
            // 
            this.btnSignUp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(78)))), ((int)(((byte)(184)))), ((int)(((byte)(206)))));
            this.btnSignUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSignUp.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignUp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnSignUp.Location = new System.Drawing.Point(24, 321);
            this.btnSignUp.Name = "btnSignUp";
            this.btnSignUp.Size = new System.Drawing.Size(250, 36);
            this.btnSignUp.TabIndex = 24;
            this.btnSignUp.Text = "Sign Up";
            this.btnSignUp.UseVisualStyleBackColor = false;
            this.btnSignUp.Click += new System.EventHandler(this.btnSignUp_Click);
            // 
            // btnGoSignIn
            // 
            this.btnGoSignIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnGoSignIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGoSignIn.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGoSignIn.ForeColor = System.Drawing.Color.White;
            this.btnGoSignIn.Location = new System.Drawing.Point(24, 363);
            this.btnGoSignIn.Name = "btnGoSignIn";
            this.btnGoSignIn.Size = new System.Drawing.Size(250, 36);
            this.btnGoSignIn.TabIndex = 18;
            this.btnGoSignIn.Text = "You already have an acount ?";
            this.btnGoSignIn.UseVisualStyleBackColor = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel3.Location = new System.Drawing.Point(24, 207);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 1);
            this.panel3.TabIndex = 23;
            // 
            // tbxMail
            // 
            this.tbxMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.tbxMail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxMail.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMail.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tbxMail.Location = new System.Drawing.Point(54, 183);
            this.tbxMail.Name = "tbxMail";
            this.tbxMail.Size = new System.Drawing.Size(200, 18);
            this.tbxMail.TabIndex = 17;
            this.tbxMail.TabStop = false;
            this.tbxMail.Text = "EMail";
            this.tbxMail.Click += new System.EventHandler(this.tbxMail_Click);
            // 
            // pbMail
            // 
            this.pbMail.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_important_mail_24;
            this.pbMail.Location = new System.Drawing.Point(24, 177);
            this.pbMail.Name = "pbMail";
            this.pbMail.Size = new System.Drawing.Size(24, 24);
            this.pbMail.TabIndex = 22;
            this.pbMail.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel2.Location = new System.Drawing.Point(24, 299);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(250, 1);
            this.panel2.TabIndex = 21;
            // 
            // tbxRePassword
            // 
            this.tbxRePassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.tbxRePassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxRePassword.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRePassword.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tbxRePassword.Location = new System.Drawing.Point(54, 275);
            this.tbxRePassword.Name = "tbxRePassword";
            this.tbxRePassword.Size = new System.Drawing.Size(200, 18);
            this.tbxRePassword.TabIndex = 15;
            this.tbxRePassword.TabStop = false;
            this.tbxRePassword.Text = "Confirme Password";
            this.tbxRePassword.Click += new System.EventHandler(this.tbxRePassword_Click);
            // 
            // pbRePassword
            // 
            this.pbRePassword.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_lock_24;
            this.pbRePassword.Location = new System.Drawing.Point(24, 269);
            this.pbRePassword.Name = "pbRePassword";
            this.pbRePassword.Size = new System.Drawing.Size(24, 24);
            this.pbRePassword.TabIndex = 20;
            this.pbRePassword.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Location = new System.Drawing.Point(24, 158);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 1);
            this.panel1.TabIndex = 19;
            // 
            // tbxUsrname
            // 
            this.tbxUsrname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.tbxUsrname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxUsrname.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxUsrname.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tbxUsrname.Location = new System.Drawing.Point(54, 134);
            this.tbxUsrname.Name = "tbxUsrname";
            this.tbxUsrname.Size = new System.Drawing.Size(200, 18);
            this.tbxUsrname.TabIndex = 13;
            this.tbxUsrname.TabStop = false;
            this.tbxUsrname.Text = "Username";
            this.tbxUsrname.Click += new System.EventHandler(this.tbxUsrname_Click);
            // 
            // pbUsrname
            // 
            this.pbUsrname.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_customer_24;
            this.pbUsrname.Location = new System.Drawing.Point(24, 128);
            this.pbUsrname.Name = "pbUsrname";
            this.pbUsrname.Size = new System.Drawing.Size(24, 24);
            this.pbUsrname.TabIndex = 16;
            this.pbUsrname.TabStop = false;
            // 
            // pbIcone
            // 
            this.pbIcone.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbIcone.BackgroundImage")));
            this.pbIcone.Location = new System.Drawing.Point(81, 18);
            this.pbIcone.Name = "pbIcone";
            this.pbIcone.Size = new System.Drawing.Size(120, 87);
            this.pbIcone.TabIndex = 14;
            this.pbIcone.TabStop = false;
            // 
            // btnBackSelectAcount
            // 
            this.btnBackSelectAcount.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.btnBackSelectAcount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBackSelectAcount.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackSelectAcount.ForeColor = System.Drawing.Color.White;
            this.btnBackSelectAcount.Location = new System.Drawing.Point(24, 405);
            this.btnBackSelectAcount.Name = "btnBackSelectAcount";
            this.btnBackSelectAcount.Size = new System.Drawing.Size(250, 36);
            this.btnBackSelectAcount.TabIndex = 25;
            this.btnBackSelectAcount.Text = "Back to select Acount";
            this.btnBackSelectAcount.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Location = new System.Drawing.Point(24, 252);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(250, 1);
            this.panel4.TabIndex = 24;
            // 
            // tbxPassword
            // 
            this.tbxPassword.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.tbxPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbxPassword.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPassword.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.tbxPassword.Location = new System.Drawing.Point(54, 228);
            this.tbxPassword.Name = "tbxPassword";
            this.tbxPassword.Size = new System.Drawing.Size(200, 18);
            this.tbxPassword.TabIndex = 22;
            this.tbxPassword.TabStop = false;
            this.tbxPassword.Text = "Password";
            this.tbxPassword.Click += new System.EventHandler(this.tbxPassword_Click);
            // 
            // pbPassword
            // 
            this.pbPassword.BackgroundImage = global::WF_cfpta_design.Properties.Resources.icons8_lock_24;
            this.pbPassword.Location = new System.Drawing.Point(24, 222);
            this.pbPassword.Name = "pbPassword";
            this.pbPassword.Size = new System.Drawing.Size(24, 24);
            this.pbPassword.TabIndex = 23;
            this.pbPassword.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(269, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 17);
            this.label1.TabIndex = 26;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // frmSignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(36)))), ((int)(((byte)(49)))));
            this.ClientSize = new System.Drawing.Size(298, 463);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.tbxPassword);
            this.Controls.Add(this.btnBackSelectAcount);
            this.Controls.Add(this.pbPassword);
            this.Controls.Add(this.btnSignUp);
            this.Controls.Add(this.btnGoSignIn);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.tbxMail);
            this.Controls.Add(this.pbMail);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tbxRePassword);
            this.Controls.Add(this.pbRePassword);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbxUsrname);
            this.Controls.Add(this.pbUsrname);
            this.Controls.Add(this.pbIcone);
            this.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmSignUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmSignUp";
            this.Load += new System.EventHandler(this.frmSignUp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbRePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbUsrname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSignUp;
        private System.Windows.Forms.Button btnGoSignIn;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox tbxMail;
        private System.Windows.Forms.PictureBox pbMail;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox tbxRePassword;
        private System.Windows.Forms.PictureBox pbRePassword;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox tbxUsrname;
        private System.Windows.Forms.PictureBox pbUsrname;
        private System.Windows.Forms.PictureBox pbIcone;
        private System.Windows.Forms.Button btnBackSelectAcount;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tbxPassword;
        private System.Windows.Forms.PictureBox pbPassword;
        private System.Windows.Forms.Label label1;
    }
}