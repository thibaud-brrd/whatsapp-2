﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WF_cfpta_design
{
    public class User
    {
        // Champs
        public string username { get; set; }
        public int password { get; set; }
        public int email { get; set; }
        public string mail { get; set; }
        public string publicKey { get; set; }
        public string isDeleted { get; set; }
    }
}